import pygame
import os
 
pygame.init()
screen = pygame.display.set_mode((720, 600))
done = False
clock = pygame.time.Clock()
x = 350
y = 400
velocity = 10
width = 200
height = 200

current_path = os.path.dirname(__file__) # Where your .py file is located
resource_path = os.path.join(current_path, 'resources') # The resource folder path
image_path = os.path.join(resource_path, 'images') # The image folder path
sound_path = os.path.join(resource_path, 'sounds')

#load images
spaceship = pygame.image.load(os.path.join(image_path, 'spaceship.png'))
bg = pygame.image.load(os.path.join(image_path, 'bg.jpg'))

#fit images
spaceship = pygame.transform.scale(spaceship, (width, height))
bg = pygame.transform.scale(bg, (720, 600))

#load title song
titleSong = pygame.mixer.music.load(os.path.join(sound_path, '01_Title Screen.mp3'))
pygame.mixer.music.set_volume(0.2)
pygame.mixer.music.play(-1) 

#load bullet sound
shot = pygame.mixer.Sound(os.path.join(sound_path, 'shoot.wav'))
shot.set_volume(0.050)

#method Shoot()
def shoot():
    shot.play()

def updateGame():
    screen.blit(bg, (0,0))
    screen.blit(spaceship, (x,y))
    pygame.display.update()

while not done:

    for event in pygame.event.get():  
        if event.type == pygame.QUIT: #end the game
                done = True
    
    keys = pygame.key.get_pressed() #checking pressed keys/held down keys
    
    if keys[pygame.K_LEFT]:
        x -= velocity
        #border left
        if x <= 0:
            x = 0
    if keys[pygame.K_RIGHT]:
        x += velocity
        #border right
        if x >= 520:
            x = 520
    
    if keys[pygame.K_SPACE]:
        shoot()

    updateGame()
    clock.tick(60)