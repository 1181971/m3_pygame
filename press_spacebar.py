#import pygame library
import pygame

pygame.init()
screen = pygame.display.set_mode((400,300))
done = False
is_green = True
x = 30
y = 30

while not done:
    for event in pygame.event.get():
        #All events come here
        #Quit game when red cross is clicked
        if event.type == pygame.QUIT:
            done = True

        if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
            is_green = not is_green

    #draw a rectangle
    #draw() function takes 3 arguments:
    #1)On which instance to draw the thing
    #2)RGB value
    #3)A rectangle instance with x,y,width,length as arguments
    pygame.draw.rect(screen, (0, 255, 0), pygame.Rect(x, y, 60, 60))

    #changing rectangle
    if is_green: color = (0,255,0)
    else: color = (255,0,0)    
    pygame.draw.rect(screen, color, pygame.Rect(x+100, y+100, 80, 80)) 

    pygame.display.flip()
    