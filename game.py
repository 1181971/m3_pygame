import os
import pygame

pygame.init()
screen_width = 720
screen_height = 600
pygame.display.set_caption("ISEP Invaderz")
screen = pygame.display.set_mode((screen_width, screen_height))
done = False
intro = False
clock = pygame.time.Clock()
score = 0

current_path = os.path.dirname(__file__)  # Where your .py file is located
# The resource folder path
resource_path = os.path.join(current_path, 'resources')
image_path = os.path.join(resource_path, 'images')  # The image folder path
sound_path = os.path.join(resource_path, 'sounds')
font_path = os.path.join(resource_path, 'fonts')

# load images
spaceship_image = pygame.image.load(os.path.join(image_path, 'spaceship.png'))
alien_image = pygame.image.load(os.path.join(image_path, 'alien.png'))
bg = pygame.image.load(os.path.join(image_path, 'bg.jpg'))

# load font
smallFont = pygame.font.Font(os.path.join(font_path, '04b_21.ttf'), 14)
bigFont = pygame.font.Font(os.path.join(font_path, '04b_21.ttf'), 80)

# fit background
bg = pygame.transform.scale(bg, (720, 600))

# load title song
titleSong = pygame.mixer.music.load(os.path.join(sound_path, '01_Title Screen.mp3'))
pygame.mixer.music.set_volume(0.1)
pygame.mixer.music.play(-1)

# load bullet sound
shotSound = pygame.mixer.Sound(os.path.join(sound_path, 'shoot.wav'))
shotSound.set_volume(0.050)
hitSound = pygame.mixer.Sound(os.path.join(sound_path, 'hitmarker.wav'))
hitSound.set_volume(0.2)

# characteristics of our main game object,controlled by the player
class player(object):
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.velocity = 10

    def draw(self, screen):
        spaceship_scaled = pygame.transform.scale(spaceship_image, (self.width, self.height))
        screen.blit(spaceship_scaled, (self.x, self.y))


class projectile(object):
    def __init__(self, x, y, radius, color):
        self.x = x
        self.y = y
        self.radius = radius
        self.color = color
        self.velocity = 5

    def draw(self, screen):
        pygame.draw.circle(screen, self.color, (self.x, self.y), self.radius)


class enemy(object):
    def __init__(self, x, y, width, height, end):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.end = end
        self.velocity = 3
        self.path = [self.x, self.end]
        self.hitbox = (self.x, self.y, self.width + 40, self.height + 10)

    def draw(self, screen):
        # first move, then draw
        self.move()
        screen.blit(alien_image, (self.x, self.y))
        # update hitbox based on movement inside draw method
        self.hitbox = (self.x, self.y, self.width + 40, self.height + 10)
        # draw hitbox
        #pygame.draw.rect(screen, (255, 0, 0), self.hitbox, 2)

    def move(self):
        # moving right
        if self.velocity > 0:
            # if next position (x + velocity) is smaller than end of path, move with velocity
            if self.x + self.velocity < self.path[1] - self.width:
                self.x += self.velocity
            else:
                # flip 180°, go to left
                self.velocity = self.velocity * -1

        # moving left
        else:
            # if next position (x - velocity) is bigger than the original "spawn point", move with velocity
            if self.x - self.velocity > self.path[0]:
                self.x += self.velocity
            else:
                # flip 180°, go to left
                self.velocity = self.velocity * -1

    def hit(self):
        hitSound.play()
        print('hit')


# intro screen
def introGame():
    intro = True
    while intro:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:  # end the game
                done = True
                pygame.quit()

        keys = pygame.key.get_pressed()  # checking pressed keys/held down keys

        if keys[pygame.K_SPACE]:
            intro = False

        screen.blit(bg, (0, 0))
        ISEPText = bigFont.render("ISEP", 1, (0, 255, 0))
        INVADERZText = bigFont.render("INVADERZ", 1, (0, 255, 0))
        PRESSText = smallFont.render("PRESS SPACE TO START", 1, (0, 255, 0))
        screen.blit(ISEPText, (200, 200))
        screen.blit(INVADERZText, (75, 350))
        screen.blit(PRESSText, (200, 500))
        pygame.display.update()

# refresh game screen


def updateGame():
    screen.blit(bg, (0, 0))
    spaceship.draw(screen)
    for bullet in bullets:
        bullet.draw(screen)

    alien.draw(screen)
    alien2.draw(screen)
    text = smallFont.render("SCORE: " + str(score), 1, (255, 0, 0))
    screen.blit(text, (390, 10))

    pygame.display.update()

# method Shoot()
def shoot():
    shotSound.play()

# main game loop
# create instance of player class
spaceship = player(350, 400, 200, 200)
# create array of bullets
bullets = []
# create instances of enemy class
alien = enemy(0,  0, 90, 90, (screen_width - 90))
alien2 = enemy(0 + alien.width, 0 + alien.height, 90, 90, (screen_width - 90))
# loop to prevent multiple bullets to fire at once
shootLoop = 0

introGame()
while not done:
    if shootLoop > 0:
        shootLoop += 1
    if shootLoop > 6:
        shootLoop = 0

    for event in pygame.event.get():
        if event.type == pygame.QUIT:  # end the game
            done = True
            pygame.quit()

    for bullet in bullets:
        # Check if bullet is within width of alien
        # Checks y value
        if bullet.y - bullet.radius < alien.hitbox[1] + alien.hitbox[3] and bullet.y + bullet.radius > alien.hitbox[1]:
            # Checks x value
            if bullet.x + bullet.radius > alien.hitbox[0] and bullet.x - bullet.radius < alien.hitbox[0] + alien.hitbox[2]:
                alien.hit()
                # increment score when hit
                score += 1

                # removes bullet from bullet list
                bullets.pop(bullets.index(bullet))

        #checks y value
        elif bullet.y - bullet.radius < alien2.hitbox[1] + alien2.hitbox[3] and bullet.y + bullet.radius > alien2.hitbox[1]:
            # Checks x value
            if bullet.x + bullet.radius > alien2.hitbox[0] and bullet.x - bullet.radius < alien2.hitbox[0] + alien2.hitbox[2]:
                alien2.hit()
                score += 1
                # removes bullet from bullet list
                bullets.pop(bullets.index(bullet))

        if bullet.y >= 0 and bullet.y < screen_height:
            bullet.y -= bullet.velocity  # shooting up means subtracting from Y
        else:
            # delete bullet because off screen
            bullets.pop(bullets.index(bullet))

    # checking pressed keys/held down keys
    keys = pygame.key.get_pressed() 

    if keys[pygame.K_LEFT]:
        spaceship.x -= spaceship.velocity
        # border left
        if spaceship.x <= 0:
            spaceship.x = 0
    if keys[pygame.K_RIGHT]:
        spaceship.x += spaceship.velocity
        # border right
        if spaceship.x >= (screen_width - spaceship.width):
            spaceship.x = (screen_width - spaceship.width)

    if keys[pygame.K_SPACE] and shootLoop == 0:
        # set bullet limit on screen
        if len(bullets) < 5:
            # add new bullet object to bullets
            bullets.append(projectile(
                round(spaceship.x + spaceship.width // 2), spaceship.y, 6, (255, 215, 0)))
            shootLoop = 1
            shoot()

        # FPS
    clock.tick(60)
    updateGame()
