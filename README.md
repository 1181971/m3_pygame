# M3_PyGame
##Getting started
<https://www.pygame.org/wiki/GettingStarted>

##Introduction
<https://www.edureka.co/blog/pygame-tutorial#z3>

Error: "Module 'pygame' has no 'init' member"
Solution: <https://stackoverflow.com/questions/50569453/why-does-it-say-that-module-pygame-has-no-init-member>

Helpful explanation object oriented game programming: <https://stackoverflow.com/questions/52010337/shooting-bullets-in-pygame>

Video series: <https://www.youtube.com/watch?v=i6xMBig-pP4&list=PLzMcBGfZo4-lp3jAExUCewBfMx3UZFkh5>

##Report
<https://docs.google.com/document/d/1LbuwkQFpjzvXPLv-yySy6LxnpbstkqAWTMizJiAZntE/edit?usp=sharing>
