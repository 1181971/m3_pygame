import pygame
import os

#construct relative path 
current_path = os.path.dirname(__file__)  # Where your .py file is located
resource_path = os.path.join(current_path, 'resources') #resource folder path
image_path = os.path.join(resource_path, 'images')  # The image folder path
sound_path = os.path.join(resource_path, 'sounds') # sound path

pygame.init()
screen = pygame.display.set_mode((500, 500))
done = False
logo = pygame.image.load(os.path.join(image_path, 'isep.png'))
shotSound = pygame.mixer.Sound(os.path.join(sound_path, 'shoot.wav'))

class player(object):
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.velocity = 2

    def draw(self, screen):
        screen.blit(logo, (self.x, self.y))

def updateGame():
    screen.fill((0,0,0))  # Fills the screen with black
    isep.draw(screen)
    pygame.display.update()


isep = player(0,0,100,100)
while not done:
    for event in pygame.event.get():
        #All events come here
        #Quit game when red cross is clicked
        if event.type == pygame.QUIT:
            done = True
            pygame.quit()
    
    keys = pygame.key.get_pressed() 

    if keys[pygame.K_LEFT]:
        isep.x -= 2
    
    if keys[pygame.K_RIGHT]:
        isep.x += 2

    if keys[pygame.K_UP]:
        isep.y -= 2
    
    if keys[pygame.K_DOWN]:
        isep.y += 2

    if keys[pygame.K_SPACE]:
        shotSound.play()

    updateGame()
